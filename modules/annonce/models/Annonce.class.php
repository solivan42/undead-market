<?php
 
namespace APP\Modules\Annonce\Models;
use APP\Modules\Base\Lib\Bdd as Bdd;
 
class Annonce extends Bdd
{
	/**
	 * Constructeur de la classe.
	 *
	 * @param  integer $nIdElement Id de l'élément.
	 *
	 * @return  void
	 */
	public function __construct($nIdElement = 0)
	{
	    parent::__construct();
	 
	    $this->aMappingChamps = array(
	        'id_annonce'        => 'nIdAnnonce',
	        'id_categorie'      => 'nIdCategorie',
	        'date_ajout'        => 'szDateAjout',
	        'titre'             => 'szTitre',
	        'description'       => 'szDescription',
	        'montant'           => 'fMontant',
	        'categorie'         => 'szCategorie',
	        'date_formatee'     => 'szDateFormatee',
	        'montant_formate'   => 'szMontantFormate',
	        'id_utilisateur'    => 'nIdUtilisateur',
	        'identifiant'       => 'sIdentifiant',
	        'code_postal'       => 'sCodePostal',
	        'commune'           => 'sCommune',
	    );
	 
	 
	    if ($nIdElement > 0) {
	        $aRecherche = array('IdAnnonce' => $nIdElement);
	        $aElements = $this->aGetElements($aRecherche);
	 
	        if (isset($aElements[0]) === true) {
	            foreach ($aElements[0] as $szCle => $szValeur) {
	                $this->$szCle = $szValeur;
	            }
	        }
	    }
	}

	/**
	 * Requête de sélection.
	 *
	 * @param array $aRecherche Critères de recherche
	 * @param string $szOrderBy Tri
	 * @param boolean $bModeCount Juste compter.
	 *
	 * @return string               Retourne la requête
	 */
	public function szGetSelect($aRecherche = array(), $szOrderBy = "", $bModeCount = false)
	{
	    if ($bModeCount === false) {
	        $szChamps = "
	            ANN.id_annonce AS nIdElement, ANN.id_annonce, ANN.id_categorie, ANN.date_ajout, ANN.titre,
	            ANN.description, ANN.montant, ANN_CAT.libelle AS categorie, ANN.code_postal, ANN.commune,
	            REPLACE(ANN.montant, '.', ',') AS montant_formate, ANN.id_utilisateur, UTI.identifiant,
	            DATE_FORMAT(ANN.date_ajout, 'le %d-%m-%Y à %H\h%i') AS date_formatee,
	            COUNT(*) AS nNbElements
	        ";
	    } else {
	        $szChamps = "
	            COUNT(*) AS nNbElements
	        ";
	    }
	 
	    $szRequete = "
	        SELECT ".$szChamps."
	        FROM annonce AS ANN
	        INNER JOIN categorie ANN_CAT ON(ANN.id_categorie = ANN_CAT.id_categorie)
	        INNER JOIN utilisateur UTI ON(ANN.id_utilisateur = UTI.id_utilisateur)
	        WHERE 1=1
	    ";
	 
	    $szRequete .= $this->szGetCriteresRecherche($aRecherche);
	 
	    if ($bModeCount === false) {
	        if ($szOrderBy != '') {
	            $szRequete .= " GROUP BY ANN.id_annonce " . $szOrderBy;
	        } else {
	            $szRequete .= " GROUP BY ANN.id_annonce ORDER BY ANN.date_ajout ASC";
	        }
	    }
	 
	    // echo "<pre>".$szRequete."</pre>";
	    // exit;
	 
	    return $szRequete;
	}


	/**
	 * Méthode permettant de compléter une requête avec des critères.
	 *
	 * @param array $aRecherche Critères de recherche
	 *
	 * @return string           Retourne le SQL des critères de recherche
	 */
	protected function szGetCriteresRecherche($aRecherche = array())
	{
	    $szRequete = '';
	 
	    // Recherche d'un élément en particulier.
	    if (isset($aRecherche['IdAnnonce']) === true && $aRecherche['IdAnnonce'] != '') {
	        $szRequete .= "
	            AND ANN.id_annonce = ".$aRecherche['IdAnnonce']."
	        ";
	    }
	 
	    // Recherche d'un élément par mots clés.
	    if (isset($aRecherche['MotsCles']) === true && $aRecherche['MotsCles'] != '') {
	        $szRequete .= "
	            AND (ANN.titre LIKE '%".$aRecherche['MotsCles']."%' OR ANN.description LIKE '".$aRecherche['MotsCles']."')
	        ";
	    }
	 
	    return $szRequete;
	}

	/**
	 * Permet de récupérer les critères de validation du formulaire d'édition.
	 *
	 * @param  string $szNomChamp Nom du champ.
	 * @param  string $szType     Type de retour (chaine ou tableau).
	 *
	 * @return string             Critères (chaine ou tableau).
	 */
	public function aGetCriteres($szNomChamp = '', $szType = 'tableau')
	{
	    $aConfig['szTitre'] = array(
	        'required' => '1',
	        'minlength' => '1',
	        'maxlength' => '255',
	    );
	    $aConfig['szDescription'] = array(
	        'required' => '1',
	        'minlength' => '1',
	    );
	    $aConfig['fMontant'] = array(
	        'required' => '1',
	        'minlength' => '1',
	        'maxlength' => '13',
	    );
	    $aConfig['nIdCategorie'] = array(
	        'required' => '1',
	        'minlength' => '1',
	        'maxlength' => '11',
	    );
	    $aConfig['sCodePostal'] = array(
	        'required' => '1',
	        'minlength' => '5',
	        'maxlength' => '5',
	    );
	    $aConfig['sCommune'] = array(
	        'required' => '1',
	        'minlength' => '1',
	        'maxlength' => '60',
	    );
	 
	    if ($szType == 'tableau') {
	        return $aConfig[$szNomChamp];
	    } elseif ($szType == 'chaine') {
	        if (isset($aConfig[$szNomChamp]) === true) {
	            return $this->szGetCriteresValidation($aConfig[$szNomChamp]);
	        }
	    }
	}

	/**
	 * Insertion d'un élément.
	 *
	 * @return boolean Retour succes ou erreur.
	 */
	public function bInsert()
	{
	    $bRetour = false;
	      
	    $szRequete = "
	        INSERT INTO annonce (id_utilisateur, id_categorie, date_ajout, titre, description, montant, code_postal, commune)
	        VALUES(".$_SESSION['nIdUtilisateur'].", '".$this->nIdCategorie."', '".date('Y-m-d H:i:s')."', '".addslashes($this->szTitre)."',
	                '".addslashes($this->szDescription)."', '".str_replace(',', '.', $this->fMontant)."',
	                '".addslashes($this->sCodePostal)."', '".addslashes($this->sCommune)."')";
	 
	    // echo "<pre>$szRequete</pre>";
	    // exit;
	 
	    $rLien = $this->rConnexion->query($szRequete);
	    $this->nIdAnnonce = $this->rConnexion->lastInsertId();
	 
	    if ($rLien) {
	        $bRetour = true;
	    }
	 
	    return $bRetour;
	}

	/**
	 * Modification d'un élément.
	 *
	 * @return boolean Retour succes ou erreur.
	 */
	public function bUpdate()
	{
	    $bRetour = false;
	 
	    $szRequete = "
	        UPDATE annonce
	        SET id_categorie = '".$this->nIdCategorie."',
	        titre = '".addslashes($this->szTitre)."',
	        description = '".addslashes($this->szDescription)."',
	        montant = '".str_replace(',', '.', $this->fMontant)."',
	        code_postal = '".$this->sCodePostal."',
	        commune = '".$this->sCommune."'
	        WHERE id_annonce = '".$this->nIdAnnonce."'";
	 
	    // echo "<pre>$szRequete</pre>";
	    // exit;
	 
	    $rLien = $this->rConnexion->query($szRequete);
	 
	    if ($rLien) {
	        $bRetour = true;
	    }
	 
	    return $bRetour;
	}

	/**
	 * Suppression d'un élément.
	 *
	 * @return boolean Retour erreur ou succès.
	 */
	public function bDelete()
	{
	    $bRetour = false;
	 
	    $szRequete = "
	        DELETE
	        FROM annonce
	        WHERE id_annonce = '".$this->nIdAnnonce."'";
	 
	    // echo "<pre>$szRequete</pre>";
	    // exit;
	 
	    $rLien = $this->rConnexion->query($szRequete);
	 
	    if ($rLien) {
	        $bRetour = true;
	    }
	 
	    return $bRetour;
	}
}