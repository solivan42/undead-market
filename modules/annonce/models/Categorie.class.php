<?php
 
namespace APP\Modules\Annonce\Models;
use APP\Modules\Base\Lib\Bdd as Bdd;
 
class Categorie extends Bdd
{
    /**
     * Constructeur de la classe.
     *
     * @param  integer $nIdElement Id de l'élément.
     *
     * @return  void
     */
    public function __construct($nIdElement = 0)
    {
        parent::__construct();
 
        $this->aMappingChamps = array(
            'id_categorie'      => 'nIdCategorie',
            'libelle'           => 'szLibelle',
        );
 
 
        if ($nIdElement > 0) {
            $aRecherche = array('IdCategorie' => $nIdElement);
            $aElements = $this->aGetElements($aRecherche);
 
            if (isset($aElements[0]) === true) {
                foreach ($aElements[0] as $szCle => $szValeur) {
                    $this->$szCle = $szValeur;
                }
            }
        }
    }
 
 
    /**
     * Requête de sélection.
     *
     * @param array $aRecherche Critères de recherche
     * @param string $szOrderBy Tri
     * @param boolean $bModeCount Juste compter.
     *
     * @return string               Retourne la requête
     */
    public function szGetSelect($aRecherche = array(), $szOrderBy = "", $bModeCount = false)
    {
        if ($bModeCount === false) {
            $szChamps = "
                CAT.id_categorie, CAT.libelle, COUNT(*) AS nNbElements
            ";
        } else {
            $szChamps = "
                COUNT(*) AS nNbElements
            ";
        }
 
        $szRequete = "
            SELECT ".$szChamps."
            FROM categorie AS CAT
            WHERE 1=1
        ";
 
        $szRequete .= $this->szGetCriteresRecherche($aRecherche);
 
        if ($bModeCount === false) {
            if ($szOrderBy != '') {
                $szRequete .= " GROUP BY CAT.id_categorie " . $szOrderBy;
            } else {
                $szRequete .= " GROUP BY CAT.id_categorie ORDER BY CAT.libelle ASC";
            }
        }
 
        // echo "<pre>".$szRequete."</pre>";
        // exit;
 
        return $szRequete;
    }
 
 
    /**
     * Méthode permettant de compléter une requête avec des critères.
     *
     * @param array $aRecherche Critères de recherche
     *
     * @return string           Retourne le SQL des critères de recherche
     */
    protected function szGetCriteresRecherche($aRecherche = array())
    {
        $szRequete = '';
 
        // Recherche d'un élément en particulier.
        if (isset($aRecherche['IdCategorie']) === true && $aRecherche['IdCategorie'] != '') {
            $szRequete .= "
                AND CAT.id_categorie = ".$aRecherche['IdCategorie']."
            ";
        }
 
        return $szRequete;
    }
}