<?php
 
namespace APP\Modules\Annonce\Controllers; // Définition du namespace de la classe
use APP\Core\Lib\Interne\PHP\AffichageHTML as AffichageHTML; // Création d’un alias pour utiliser la classe d’affichage
 
class AnnonceAdminHTML extends AffichageHTML
{
    /**
     * Récupère le contenu central de la page.
     *
     * @return string $szContenu Contenu HTML
     */
    public function szGetContenuCentralHTML()
    {
        $szContenu = $this->szGetContenuEnCacheHTML();
     
        if ($szContenu == '') {
      
            // Ajout de la classe JS pour la déconnexion etc.
            $szFichierJS = $this->szGetFichierPourInclusion('modules', 'authentification/JS/Authentification.js', 'url');
            $this->bSetScriptJavascript($szFichierJS);
             
            if ($this->szZone == 'site') {
                // Insertion de la classe JS spécifique à la partie publique.
                $szFichierJS = $this->szGetFichierPourInclusion('modules', 'annonce/JS/AnnoncePublic.js', 'url');
                $this->bSetScriptJavascript($szFichierJS);
            }
            if ($this->szZone == 'admin') {
                // Insertion de la classe JS spécifique à la partie admin.
                $szFichierJS = $this->szGetFichierPourInclusion('modules', 'annonce/JS/AnnonceAdmin.js', 'url');
                $this->bSetScriptJavascript($szFichierJS);
            }
     
            // Ajout de la classe JS d'amorce du module.
            $szFichierJS = $this->szGetFichierPourInclusion('modules', 'annonce/JS/Annonce.js', 'url');
            $this->bSetScriptJavascript($szFichierJS);
     
            // Modes à traiter.
            $aModesAcceptes = array(
                'liste', 'form_recherche', 'form_edition'
            );
     
            // Modes pour lesquels un retour simple de vue / formulaire n'est pas suffisant.
            $aModesSpecifiques = array();
     
            // On associe une classe avec une classe de data pour récupérer
            // les critères de validation.
            $aFormulaires = array(
                'form_edition' => 'Annonce',
            );
     
            if (in_array($this->szMode, $aModesAcceptes) === true) {
     
                // Le mode demandé est à traiter.
     
                if (array_key_exists($this->szMode, $aModesSpecifiques) === true) {
     
                    // Si un retour simple de vue n'est pas suffisant pour le mode
                    // on appelle une méthode particulière.
     
                } else {
     
                    // Si un retour de vue suffit, on récupère automatiquement
                    // le contenu de la vue en fonction du mode.
     
                    if (array_key_exists($this->szMode, $aFormulaires) === true) {
     
                        // Récupération d'un formulaire avec critères de validation.
                        $szContenu = $this->szGetVueAvecValidationFormulaire($aFormulaires[$this->szMode]);
     
                    } else {
     
                        // Récupération d'une vue simple.
                        $szContenu = $this->szGetContenueVueMode();
     
                    }
     
                }
     
            } else {
     
                // Le mode n'est pas à traiter, on affiche la page d'accueil.
                $szContenuAccueil = $this->szGetContenuFichierHTML($this->szGetFichierPourInclusion('modules', 'annonce/vues/accueil.html'));
                $szContenu = $this->szGetPageAccueilHTML($szContenuAccueil);
     
            }
     
            $this->vSauvegardeContenuEnCache($szContenu);
     
        }
     
        return $szContenu;
    }

    /**
     * Recherche d'annonces dans la base de données.
     *
     * @return array Informations.
     */
    private function aGetAnnonces()
    {
        $aRetour = array(
            'bSucces'       => false,   // Succès ou échec de l'opération.
            'szErreur'      => '',      // Message d'erreur en cas d'échec.
            'szMessage'     => '',      // Message de succès en cas de succès.
            'aElements'     => array(), // Eléments trouvés.
            'nNbElements'   => 0,       // Nombre d'éléments.
        );
     
        // Nombre d'éléments à afficher par page.
        $nNbElementsParPage = 10;
     
        // Critères de recherche.
        $aRecherche = array();
     
        // Tri des résultats.
        $szOrderBy = '';
        if (isset($_REQUEST['szOrderBy']) === true) {
            $szOrderBy = 'ORDER BY '.$_REQUEST['szOrderBy'];
        }
     
        // Instanciation de la classe de data.
        $oElement = $this->oNew('Annonce');
     
        // Récupération de la pagination.
        $oPagination = $this->oGetInfosPagination($oElement, $aRecherche, $nNbElementsParPage);
        $aRetour['aPagination'] = $oPagination;
     
        // Récupération des annonces présentes dans la base de données.
        $aRetour['aElements'] = $oElement->aGetElements($aRecherche, $oPagination->nStart, $nNbElementsParPage, $szOrderBy);
     
        // Compte des annonces présentes dans la base de données.
        $aRetour['nNbElements'] = count($aRetour['aElements']);
     
        return $aRetour;
    }
}