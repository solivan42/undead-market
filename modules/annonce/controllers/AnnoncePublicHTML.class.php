<?php
 
namespace APP\Modules\Annonce\Controllers; // Définition du namespace de la classe
use APP\Core\Lib\Interne\PHP\AffichageHTML as AffichageHTML; // Création d’un alias pour utiliser la classe d’affichage
 
class AnnoncePublicHTML extends AffichageHTML
{
    /**
     * Récupère le contenu central de la page.
     *
     * @return string $szContenu Contenu HTML
     */
    public function szGetContenuCentralHTML()
    {
        $szContenu = $this->szGetContenuEnCacheHTML();
 
        if ($szContenu == '') {
 
            // Le fichier JS spécifique à la zone publique.
            $szFichierJS = $this->szGetFichierPourInclusion('modules', 'annonce/JS/AnnoncePublic.js', 'url');
            $this->bSetScriptJavascript($szFichierJS);
 
            // Le fichier JS de routage vers le bon JS (zone d'admin, zone publique).
            $szFichierJS = $this->szGetFichierPourInclusion('modules', 'annonce/JS/Annonce.js', 'url');
            $this->bSetScriptJavascript($szFichierJS);
 
            // Ajout de la feuille de style spécifique à notre module.
            $szFichierCSS = $this->szGetFichierPourInclusion('modules', 'annonce/templates/defaut/themes/defaut/CSS/style.css', 'url');
            $this->bSetFeuilleStyle($szFichierCSS);
 
            // Modes à traiter.
            $aModesAcceptes = array(
                'liste_publique'
            );
 
            // Modes pour lesquels un retour simple de vue / formulaire n'est pas suffisant.
            $aModesSpecifiques = array();
 
            // On associe une classe avec une classe de data pour récupérer
            // les critères de validation.
            $aFormulaires = array();
 
            if (in_array($this->szMode, $aModesAcceptes) === true) {
 
                // Le mode demandé est à traiter.
 
                if (in_array($this->szMode, $aModesSpecifiques) === true) {
 
                    // Si un retour simple de vue n'est pas suffisant pour le mode
                    // on appelle une méthode particulière.
 
 
                } else {
 
                    // Si un retour de vue suffit, on récupère automatiquement
                    // le contenu de la vue en fonction du mode.
 
                    if (array_key_exists($this->szMode, $aFormulaires) === true) {
 
                        // Récupération d'un formulaire avec critères de validation.
                        $szContenu = $this->szGetVueAvecValidationFormulaire($aFormulaires[$this->szMode]);
 
                    } else {
 
                        // Récupération d'une vue simple.
                        $szContenu = $this->szGetContenueVueMode();
 
                    }
 
                }
 
            } else {
 
 
                switch ($this->szMode) {
                    case 'accueil':
                        // Petit cas particulier : nous n'avons pas de vue appelée "accueil"
                        // car ce nom est trop générique, nous l'avons appelée "liste_publique.html".
                        // Du coup, si nous appelons "accueil" nous renvoyons la vue "liste_publique.html".
                        $szContenu = $this->szGetContenuFichierHTML($this->szGetFichierPourInclusion('modules', 'annonce/vues/liste_publique.html'));
                        break;
                     
                    default:
                        // Par défaut, on renvoie le contenu de l'accueil.
                        $szContenu = $this->szGetPageAccueilHTML();
                        break;
                }
 
            }
 
            $this->vSauvegardeContenuEnCache($szContenu);
 
        }
 
        return $szContenu;
    }
}