<?php
 
namespace APP\Modules\Annonce\Controllers;
use APP\Core\Lib\Interne\PHP\UndeadBrain as UndeadBrain;
 
class AnnonceAdminAction extends UndeadBrain
{
	/**
	 * Constructeur de la classe.
	 *
	 * @param  string $szAction Action à effectuer.
	 *
	 * @return  void
	 */
	public function __construct($szAction = '')
	{
	    // On regarde si du contenu est disponible en cache.
	    $szContenuEnCache = $this->szGetContenuEnCache();
	 
	    if ($szContenuEnCache != '') {
	 
	        // Si du contenu est disponible en cache, on le renvoie.
	        echo $szContenuEnCache;
	 
	    } else {
	        // Si aucun contenu n'est en cache, on traite l'action demandée.
	        if ($szAction == 'edition') {
	 
	            $nIdElement = 0;
	            if (isset($_REQUEST['nIdElement']) === true) {
	                $nIdElement = $_REQUEST['nIdElement'];
	            }
	            $szRetour = $this->szDynamisationFormulaireEdition($nIdElement);
	 
	        } elseif ($szAction == 'enregistrement_edition') {
	 
	            $nIdElement = 0;
	            if (isset($_REQUEST['nIdElement']) === true) {
	                $nIdElement = $_REQUEST['nIdElement'];
	            }
	            $szRetour = $this->szEnregistrementAnnonce($nIdElement);
	 
	        } elseif ($szAction == 'suppression') {
	 
	            $nIdElement = 0;
	            if (isset($_REQUEST['nIdElement']) === true) {
	                $nIdElement = $_REQUEST['nIdElement'];
	            }
	            $szRetour = $this->szSuppressionAnnonce($nIdElement);
	 
	        } elseif ($szAction == 'recherche') {
	 
	            // Recherche d'annonces.
	            $aRetour = $this->aGetAnnonces();
	            $szRetour = json_encode($aRetour);
	 
	        }

	        echo $szRetour;
	 
	        // Sauvegarde du contenu dans le cache.
	        $this->vSauvegardeContenuEnCache($szRetour);
	 
	    }
	 
	}

	/**
	 * Recherche d'annonces dans la base de données.
	 *
	 * @return array Informations.
	 */
	private function aGetAnnonces()
	{
	    $aRetour = array(
	        'bSucces'       => false,   // Succès ou échec de l'opération.
	        'szErreur'      => '',      // Message d'erreur en cas d'échec.
	        'szMessage'     => '',      // Message de succès en cas de succès.
	        'aElements'     => array(), // Eléments trouvés.
	        'nNbElements'   => 0,       // Nombre d'éléments.
	    );
	 
	    // Nombre d'éléments à afficher par page.
	    $nNbElementsParPage = 10;
	 
	    // Critères de recherche.
	    $aRecherche = array();
	 
	    if (isset($_REQUEST['szMotsClesRch']) === true) {
	        $aRecherche['MotsCles'] = $_REQUEST['szMotsClesRch'];
	    }
	 
	    // Tri des résultats.
	    $szOrderBy = '';
	    if (isset($_REQUEST['szOrderBy']) === true) {
	        $szOrderBy = 'ORDER BY '.$_REQUEST['szOrderBy'];
	    }
	 
	    // Instanciation de la classe de data.
	    $oElement = $this->oNew('Annonce');
	 
	    // Récupération de la pagination.
	    $oPagination = $this->oGetInfosPagination($oElement, $aRecherche, $nNbElementsParPage);
	    $aRetour['aPagination'] = $oPagination;
	 
	    // Récupération des annonces présentes dans la base de données.
	    $aRetour['aElements'] = $oElement->aGetElements($aRecherche, $oPagination->nStart, $nNbElementsParPage, $szOrderBy);
	 
	    // Compte des annonces présentes dans la base de données.
	    $aRetour['nNbElements'] = count($aRetour['aElements']);
	 
	    return $aRetour;
	}

	/**
	 * Dynamisation du formulaire d'édition d'une annonce.
	 *
	 * @param  integer $nIdElement  Id de l'annonce.
	 *
	 * @return string               Retour JSON.
	 */
	private function szDynamisationFormulaireEdition($nIdElement = 0)
	{
	    $aRetour = array(
	        'aElements' => array(),
	        'aSelects'  => array(),
	    );
	 
	    $oCategorie = $this->oNew('Categorie');
	 
	    $aCategories = $oCategorie->aGetElements();
	 
	    $aOptions = array();
	    foreach ($aCategories as $nIndex => $oElement) {
	        $aOptions[] = array('valeur' => $oElement->nIdCategorie, 'libelle' => $oElement->szLibelle);
	    }
	 
	    $aRetour['aSelects']['nIdCategorie'] = $aOptions;
	 
	    if ($nIdElement > 0) {
	        // Instanciation de la classe de data.
	        $oElement = $this->oNew('Annonce', array($nIdElement));
	 
	        $aRetour['aElements'][] = $oElement;
	    }
	 
	    return json_encode($aRetour);
	}

	/**
	 * Enregistrement de l'annonce.
	 *
	 * @param  integer $nIdElement  Id de l'annonce.
	 *
	 * @return string Retour JSON.
	 */
	private function szEnregistrementAnnonce($nIdElement = 0)
	{
	    $aRetour = array(
	        'bRetour'   => false,
	        'szSucces'  => '',
	        'szErreur'  => '',
	    );
	 
	    $aParamsConstruct = array();
	    if ($nIdElement > 0) {
	        $aParamsConstruct = array($nIdElement);
	    }
	 
	    $oElement = $this->oNew('Annonce', $aParamsConstruct);
	 
	    $oElement->nIdCategorie = $_REQUEST['nIdCategorie'];
	    $oElement->szTitre = $_REQUEST['szTitre'];
	    $oElement->szDescription = $_REQUEST['szDescription'];
	    $oElement->fMontant = $_REQUEST['fMontant'];
	    $oElement->sCodePostal = $_REQUEST['sCodePostal'];
	    $oElement->sCommune = $_REQUEST['sCommune'];
	 
	    if ($nIdElement > 0) {
	        $aRetour['bSucces'] = $oElement->bUpdate();
	    } else {
	        $aRetour['bSucces'] = $oElement->bInsert();
	    }
	 
	    if ($aRetour['bSucces'] === false) {
	        $aRetour['szErreur'] = "Une erreur est survenue lors de l'enregistrement de votre annonce.";
	    } else {
	        $aRetour['szSucces'] = "Votre annonce a été enregistrée avec succès.";
	    }
	 
	    return json_encode($aRetour);
	}

	/**
	 * Suppression de l'annonce.
	 *
	 * @param  integer $nIdElement  Id de l'annonce.
	 *
	 * @return string Retour JSON.
	 */
	private function szSuppressionAnnonce($nIdElement = 0)
	{
	    $aRetour = array(
	        'bRetour'   => false,
	        'szSucces'  => '',
	        'szErreur'  => '',
	    );
	 
	    $oElement = $this->oNew('Annonce', array($nIdElement));
	 
	    $aRetour['bSucces'] = $oElement->bDelete();
	 
	    if ($aRetour['bSucces'] === false) {
	        $aRetour['szErreur'] = "Une erreur est survenue lors de la suppression de votre annonce.";
	    } else {
	        $aRetour['szSucces'] = "Votre annonce a été supprimée avec succès.";
	    }
	 
	    return json_encode($aRetour);
	}
}