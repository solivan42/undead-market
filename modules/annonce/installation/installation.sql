CREATE TABLE IF NOT EXISTS `annonce` (
  `id_annonce` int(11) NOT NULL AUTO_INCREMENT,
  `id_utilisateur` int(11) NOT NULL,
  `id_categorie` int(11) NOT NULL,
  `date_ajout` datetime NOT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `montant` float(10,2) DEFAULT NULL,
  `code_postal` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `commune` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_annonce`),
  KEY `id_categorie` (`id_categorie`),
  KEY `id°utilisateur` (`id_utilisateur`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;
 
INSERT INTO `annonce` (`id_annonce`, `id_utilisateur`, `id_categorie`, `date_ajout`, `titre`, `description`, `montant`, `code_postal`, `commune`) VALUES
(1, 1, 1, '2017-06-21 17:50:01', 'Cerveau un peu moisi', 'Un cerveau un peu moisi. Je ne mange que du frais ! Chacun son truc !', 15.00, '42000', 'ST ETIENNE'),
(5, 1, 2, '2017-06-22 09:55:58', 'Un bras un peu rongé', 'Un bras que j''ai un peu rongé lors d''une fringale... désolé.', 90.00, '42000', 'ST ETIENNE'),
(6, 1, 1, '2017-07-02 18:15:59', 'Oeil en très bon état', 'Un oeil "trouvé"... par terre... il y a deux jours.', 130.00, '42270', 'ST PRIEST EN JAREZ');
  
CREATE TABLE IF NOT EXISTS `categorie` (
  `id_categorie` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_categorie`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;
 
INSERT INTO `categorie` (`id_categorie`, `libelle`) VALUES
(1, 'Organes internes'),
(2, 'Membres'),
(3, 'Armes de poing'),
(4, 'Armes à feu');
