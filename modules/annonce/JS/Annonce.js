var Annonce = function ()
{
    if (szZoneCourante == 'site') {
        // Zone publique.
        AnnoncePublic.apply(this, arguments);
    } else if (szZoneCourante == 'application') {
        // Zone privée.
        AnnoncePrive.apply(this, arguments);
    } else {
        // Zone d'admin.
        AnnonceAdmin.apply(this, arguments);
    }
  
  
  
    /**
     * Document Ready
     * Tout ce qui est ajouté ici sera automatiquement appelé au chargement.
     *
     * @return {void}
     */
    this.vInit = function()
    {
        if (szZoneCourante == 'site') {
            // Zone publique.
 
 
            // Exécution manuelle de l'action permettant de récupérer la liste.
            this.vExecuteAction('', 'annonce', 'btn_liste_publique');
        } else if (szZoneCourante == 'application') {
            // Zone privée.
             
        } else {
            // Zone d'admin. : Récupération du HTML de la liste des annonces.
            this.vGetListeAnnoncesHTML();
        }
    };
};