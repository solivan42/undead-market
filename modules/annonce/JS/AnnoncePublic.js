var AnnoncePublic = function ()
{
    Core.apply(this, arguments);
 
 
   /**
     * Insertion de la liste au format HTML.
     *
     * @param  {string} szReponseHTML Code HTML.
     * @param  {object} oParams       Paramètres.
     *
     * @return {void}
     */
    this.vInsereListeAnnoncesPubliqueHTML = function(szReponseHTML, oParams)
    {
        // alert(szReponseHTML);
        $('#contenu').html(szReponseHTML);
 
        // Chargement des évènements du coeur.
        this.vChargeEvenementsGeneraux();
 
        // Dynamisation de la liste des annonces.
        var oParams = {
            aVariables: [0]
        };
        this.vExecuteAction('', 'annonce', 'btn_get_annonces', oParams);
    };

    /**
     * Dynamisation de la liste des annonces publique.
     *
     * @param  {string} oReponseJSON    Infos au format JSON.
     * @param  {object} oParams         Paramètres.
     *
     * @return {void}
     */
    this.vDynamiseListeAnnoncesPubliqueJSON = function(oReponseJSON, oParams)
    {
        // On clone le modèle de vignette d'annonce.
        var eUneAnnonceTemp = $('.une_annonce.clone').clone();
        // On supprime la classe "clone" du clone afin de ne conserver qu'un modèle.
        eUneAnnonceTemp.removeClass('clone');
 
        for (i = 0; i < oReponseJSON.aElements.length; i++) {
 
            // Pour chaque élément trouvé dans la base de donnée...
 
            var oUneAnnonce = oReponseJSON.aElements[i];
 
            // On crée une copie afin de pouvoir la modifier.
            var eUneAnnonce = eUneAnnonceTemp.clone();
 
            // On injecte toutes les valeurs de notre objets trouvé en base.
            eUneAnnonce.find('.titre').text(oUneAnnonce.szTitre);
            eUneAnnonce.find('.categories').text(oUneAnnonce.szCategorie);
            eUneAnnonce.find('.szDateAjout').text(oUneAnnonce.szDateFormatee);
            eUneAnnonce.find('.fMontant').text(oUneAnnonce.szMontantFormate);
            eUneAnnonce.find('.szProposePar').text(oUneAnnonce.sIdentifiant);
            eUneAnnonce.find('.ville_annonce').text(oUneAnnonce.sCodePostal+' '+oUneAnnonce.sCommune);
 
            // Insertion de notre clone dans la liste des annonces.
            $('.liste_annonces').append(eUneAnnonce);
        }
    }

};