var AnnonceAdmin = function ()
{
     // La classe du module hérite de la classe Recherche.
    Recherche.apply(this, arguments);
 
    // On stocke l'objet courant dans une variable.
    var oThat = this;
 
    /**
     * Document Ready
     * Tout ce qui est ajouté ici sera automatiquement appelé au chargement.
     *
     * @return {void}
     */
    this.vInit = function()
    {
        // Récupération du HTML de la liste des annonces.
        this.vGetListeAnnoncesHTML();
    };
 
 
    /**
     * Récupération de la liste au format HTML.
     *
     * @return {void}
     */
    this.vGetListeAnnoncesHTML = function()
    {
        // On exécute l'action permettant de récupérer le HTML de la liste d'annonces.
        this.vExecuteAction('', 'annonce', 'btn_liste_annonces');
    };
      
    /**
     * Insertion de la liste au format HTML non dynamisée dans la zone de résultats.
     *
     * @param  {string} szReponseHTML Code HTML.
     * @param  {object} oParams       Paramètres.
     *
     * @return {void}
     */
    this.vInsereListeAnnoncesHTML = function(szReponseHTML, oParams)
    {
        // Affichage du titre dans le fil d'Ariane.
        this.vAfficheFilAriane('<h1>Gestion des annonces</h1>');
     
        // On vide la zone de filtre (recherche).
        $('#zone_navigation_2').html('');
     
        // On insère la liste non dynamisée.
        $('#zone_navigation_3').html(szReponseHTML);
     
        // On affiche la zone centrale en mode recherche + résultats.
        vAffichageRecherche();
     
        // Chargement des évènements du coeur.
        this.vChargeEvenementsGeneraux();
     
        // Dynamisation de la liste des annonces.
        this.vDynamiseListeAnnonces();
    };
     
    /**
     * Dynamisation de la liste des annonces.
     *
     * @return {void}
     */
    this.vDynamiseListeAnnonces = function()
    {
        this.vChargeListe('annonce', $('#liste_annonce'));
    };

    /**
     * Récupération du HTML du formulaire de recherche.
     *
     * @return {void}
     */
    this.vGetFiltreHTML = function()
    {
        var szZoneFiltre = $('#zone_navigation_2').html();
     
        // Si la zone de filtre n'a pas été chargée, on la charge.
        if (szZoneFiltre == '') {
            // On exécute l'action permettant de récupérer le HTML du formulaire de recherche.
            this.vExecuteAction('', 'annonce', 'btn_recherche_annonces_html');
        }
    };

    /**
     * Insertion du formulaire de recherche au format HTML non dynamisée
     * dans la zone de filtre.
     *
     * @param  {string} szReponseHTML Code HTML.
     * @param  {object} oParams       Paramètres.
     *
     * @return {void}
     */
    this.vInsereFormulaireRechercheHTML = function(szReponseHTML, oParams)
    {
        // On insère le formulaire de recherche.
        $('#zone_navigation_2').html(szReponseHTML);
     
        // Appel des listeners sur les champs de recherche.
        this.vChargeEvenementsChampsRecherche('annonce');
    };

    /**
     * Dynamisation du formulaire d'édition.
     *
     * @param  {string} oReponseJSON    Infos au format JSONL.
     * @param  {object} oParams         Paramètres.
     *
     * @return {void}
     */
    this.vDynamisationFormulaireEditionJSON = function(oReponseJSON, oParams)
    {
        var oCallback = this.oGetFonctionCallback(this, this.vOuvreCalqueEdition);
        this.vChargeFormulaireData(oReponseJSON, oParams, oCallback);
    };
     
    /**   
     * Ouverture du calque d'édition d'une annonce.
     *
     * @param  {string} oReponseJSON    Infos au format JSONL.
     *
     * @return {void}
     */
    this.vOuvreCalqueEdition = function(oReponseJSON)
    {
        var nIdElement = 0;
      
        // Si on a un id en paramètre, on est en modif.
        if (typeof oReponseJSON.aElements[0] != 'undefined') {
            if (typeof oReponseJSON.aElements[0].nIdElement != 'undefined') {
                    nIdElement = oReponseJSON.aElements[0].nIdElement;
            }
        }
     
        // Ajout de l'id de l'annonce sur le bouton.
        $('.btn_enregistre_annonce').addClass('variable_1_'+nIdElement);
     
        // Ouverture du calque.
        vOuvreCalque('modal_calque_edition_annonce');
     
        // Chargement des évênements généraux (ex: clic bouton fermeture).
        vLoad();
    };

    /**
     * Callback exécutée suite à l'enregistrement d'une annonce.
     *
     * @param  {string} oReponseJSON    Infos au format JSONL.
     * @param  {object} oParams         Paramètres.
     *
     * @return {void}
     */
    this.vCallbackEnregistrementAnnonce = function(oReponseJSON, oParams)
    {
        // Fermeture du calque.
        vFermeCalque('modal_calque_edition_annonce');
      
        // Rafraîchissement de la liste.
        this.vChargeListe('annonce', $('#liste_annonce'));
    };

    /**
     * Callback de suppression d'une annonce.
     *
     * @param  {string} oReponseJSON    Infos au format JSONL.
     * @param  {object} oParams         Paramètres.
     *
     * @return {void}
     */
    this.vCallbackSuppressionAnnonce = function(oReponseJSON, oParams)
    {
        // Rafraîchissement de la liste.
        this.vChargeListe('annonce', $('#liste_annonce'));
    };
};